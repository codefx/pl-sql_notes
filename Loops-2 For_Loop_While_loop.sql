--For Loop
--set SERVEROUTPUT ON;
BEGIN
    FOR num IN 3..15
     LOOP
      DBMS_OUTPUT.PUT_LINE('Your Turn is : '|| num);
     END LOOP;
END;

-- Reverse For Loop
BEGIN
    FOR num IN REVERSE 1..10
     LOOP
      DBMS_OUTPUT.PUT_LINE('THE DISK will self-destructed in '|| num||' seconds.');
     END LOOP;
     DBMS_OUTPUT.PUT_LINE('Impossible Mission is completed.');
END;
----
DECLARE
    v_start NUMBER := 40;
    v_stop NUMBER := 60;
BEGIN
    FOR v IN v_start .. v_stop  -- remember first and last value included.
     LOOP
      DBMS_OUTPUT.PUT_LINE('v is : '|| v);
     END LOOP;
END;
---
TRUNCATE TABLE T_Temp;

DECLARE
    v_start NUMBER := 100;
    v_stop NUMBER := 150;
BEGIN
    FOR v IN v_start .. v_stop  -- remember first and last value included.
     LOOP
      INSERT INTO T_Temp VALUES(v);
     END LOOP;
END;

select * from T_Temp;

------------- WHILE LOOP----------------------
DECLARE
 num PLS_INTEGER := 555;
BEGIN
    WHILE num <656    -- remember first value not included
     LOOP
      num := num+1;
      DBMS_OUTPUT.PUT_LINE('Your number is : '|| num);
     END LOOP;
END;
-------------
TRUNCATE TABLE T_Temp;

DECLARE
 num PLS_INTEGER := 555;
BEGIN
    WHILE num <655    -- remember first value not included
     LOOP
      num := num+1;
      INSERT INTO T_Temp VALUES(num);
     END LOOP;
END;
select * from T_Temp;
