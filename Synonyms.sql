create synonym isler  --tablonun gerçek adını ve objenin
for JOBS;             --sahibini göremez.


select * from isler;


CREATE PUBLIC SYNONYM UPIsler  -- tabloyu/synonym i diğer 
for JOBS;                   --kullanıcılara da açmak için

drop synonym isler;
drop PUBLIC SYNONYM UPIsler;