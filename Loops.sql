--LOOPS

-- LOOP
-- FOR LOOP
-- WHILE LOOP
--SET SERVEROUTPUT ON;
--SET FEEDBACK OFF;
DECLARE
    num NUMBER :=1;
BEGIN
    LOOP
    DBMS_OUTPUT.PUT_LINE('NUMBER is : '|| num);
    num := num+1;
    END LOOP;
END;
----
DECLARE
    num NUMBER :=1;
BEGIN
    LOOP
     DBMS_OUTPUT.PUT_LINE('NUMBER is : '|| num);
     num := num+1;
     IF num > 200  
      THEN
       DBMS_OUTPUT.PUT_LINE('Exiting Loop..');
      EXIT;
     END IF;
    END LOOP;
END;
-------------------

Create Table T_Temp
(
  T_ID INT
);

DECLARE
    j PLS_INTEGER :=0;
BEGIN
    LOOP     
     j := j+1;
     INSERT INTO T_Temp VALUES(j);
     IF j > 80 THEN       
      EXIT;
     END IF;
    END LOOP;
END;

select * from T_Temp;