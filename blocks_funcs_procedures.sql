--BLOCKs

--Normal
--Function
--Procedure

--NORMAL BLOCK

DECLARE --optional(if you use variable, use this section)
--variable definitions
    degerID NUMBER := 100;  -- >> value assignment " := " , default assignment, setting operation
BEGIN

    SELECT ... WHERE sutunID = degerID  -- " = " --> Comparison
    EXCEPTION -- optional(exception handling)

END;
----------------------------------------------------

--FUNCTION BLOCK

FUNCTION FindUser(userID INTEGER) RETURN NUMBER IS

    UValue NUMBER;

BEGIN 

    SELECT .....
    INTO UValue
    FROM Table_Name
    WHERE ColumnID = userID

    RETURN UValue;

END FindUser;
-----------------------------------------------------

--PROCEDURE BLOCK

PROCEDURE DeleteUSER(toDeleteUserID IN NUMBER)
IS
    canDefineVariableArea NUMBER;
BEGIN
    DELETE FROM Table_USERs WHERE USER_ID = toDeleteUserID;
END;

---------------------------------------
--DEFINING VARIABLES

UserID NUMBER;
UserID NUMBER :=100;


UserName VARCHAR2(25) := 'Unknown';

PI CONSTANT NUMERIC(2,2) := 3.14;

------------SAMPLES--------------------------------

SET SERVEROUTPUT ON; -- if you wish to see the output
DECLARE 
    Name VARCHAR2(25) := 'UnKnown';
BEGIN
    DBMS_OUTPUT.PUT_LINE('My name is '|| Name); --Included Packages
    Name := 'CodeFX';
    DBMS_OUTPUT.PUT_LINE('My name is '|| Name);
END;
 

------------------------Anonymous block---
BEGIN
    IF 3 < 5   THEN
        DBMS_OUTPUT.PUT_LINE('3 is smaller then 5')
    ELSIF 3 < 4 THEN
        DBMS_OUTPUT.PUT_LINE('3 is smaller then 4')
    ELSE
        DBMS_OUTPUT.PUT_LINE('3 is not smaller then 5 and 4')
    END IF;
END;











 














