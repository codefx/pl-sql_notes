create or replace VIEW vw_EmployeDepMinMaxAvgSalary
(
 Name, MinSal, MaxSal, AvgSal
)
as
SELECT d.department_name, MIN(E.Salary), MAX(E.Salary), floor(AVG(E.Salary))
FROM Employees e, departments d
where e.department_id = d.department_id
group by d.department_name;

select * from vw_EmployeDepMinMaxAvgSalary 