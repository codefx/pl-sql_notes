/*
order by genel sql ile aynı
select sutun/lar
from tablo
order by sutun/lar asc/desc
*/
select first_name
from employees
order by  last_name desc;

SELECT first_name,last_name,DEPARTMENT_ID,salary
FROM EMPLOYEES
order by DEPARTMENT_ID ,salary desc;

SELECT 6489/365 FROM DUAL;

select first_name||' '||last_name "Name", hire_date,trunc((CURRENT_DATE - TO_DATE(hire_date))/365) as kidemYIL
from EMPLOYEES
order by hire_date;

select first_name||' '||last_name "Name", hire_date,trunc((CURRENT_DATE - TO_DATE(hire_date))) as kidemGUN
from EMPLOYEES
order by hire_date;

select first_name||' '||last_name "Name", hire_date,trunc((CURRENT_DATE - TO_DATE(hire_date))) as kidemGUN
from EMPLOYEES
order by 1; --çağırdığımız sutunların 1.sine göre

select first_name||' '||last_name "Name", hire_date,trunc((CURRENT_DATE - TO_DATE(hire_date))) as kidemGUN
from EMPLOYEES
order by 3; --çağırdığımız sutunların 3.süne göre



SELECT TO_CHAR(CURRENT_DATE, 'HH24SP:MISP:SSSP')
FROM DUAL;


