select * from EMPLOYEES;
select * from employees where hire_date='07/02/2007';

--upper() lower()
select UPPER(first_name), LOWER(last_name) from employees;

--INITCAP : baş harf büyük
select INITCAP(first_name), INITCAP(last_name) from employees;

--CONCAT : birleştirme  aynı zamanda + değil || pipe ile
select CONCAT(first_name, ' ' || last_name) from employees;

--selecten sonra olmayan bir tablodan işlem yapacaksan DUAL
-- T-SQL deki sadece select içeren sorgular için burada "from dual" ekle
select concat('codefx','cd') from dual; -- codefxcd
select 5+4 from dual; --  9

--LENGTH malum;
select length('codefx') from dual;

--INSTR (bir metinde, verilen değeri, indisten, kaçıncı bulunacak) 
select instr('Merhaba Oracle', 'r',1,2)from dual;  --2. r yi bul

--SUBSTR (bir metinden, indisten başla, bukadar karakter)getir
select substr('İzmir',2,3) from dual;
select substr('İzmir',-4,3) from dual;

--LPAD, RPAD left veya right verilen karakterle verilen sayıya kadar verilen yönden doldur.
select RPAD('.izmir',10,'*') from dual;  -- .izmir****
select LPAD(RPAD('.izmir',10,'*'),15,'*') from dual;  -- *****.izmir****

--RTRIM,LTRIM,
select LTRIM(LPAD(RPAD('.izmir',10,'*'),15,'*'),'*') from dual; -- .izmir****
select RTRIM(LTRIM(LPAD(RPAD('.izmir',10,'*'),15,'*'),'*'),'*') from dual; -- .izmir
select LTRIM(RTRIM(LTRIM(LPAD(RPAD('.izmir',10,'*'),15,'*'),'*'),'*'),'.') from dual; -- izmir

-- TRIM : verileni FROM metin temizle 
select trim('*' from '*-codefx-*') from dual;
select trim('-' from trim('*' from '*-codefx-*'))from dual;
--sadece bir taraftanın diğer şekilleri
select trim(trailing '-' from trim('*' from '*-codefx-*'))from dual; --sadece sağdan
select trim(leading '-' from trim('*' from '*-codefx-*'))from dual;  -- sadece soldan

--ASCII 
select ascii('A') from dual;
--CHR
select chr(65) from dual;

-- denemeler
select 'First name : ' || first_name||' '|| upper(Last_name) ||' '||'''s Job ID : ' || lower(Job_id)
from EMPLOYEES;
-- kaçış karakteri:  ' için gereken yerde iki tane kullanmak gerekiyor. yukarıda 's Job ID diyebilmek için 2 tane ekledik
-- birinci concat için sonraki iki kesme kaçış karakteri.toplam 3 gibi oldu: '''s Job ID : '

