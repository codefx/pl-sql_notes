--
CREATE OR REPLACE function GET_SAL2(EMP_ID Employees.Employee_ID%TYPE) -- type "function" lowercase, SqlDeveloper BUG!!!
RETURN NUMBER                                                         -- or "I" to "i", this is bug.
IS
    SAL employees.salary%TYPE :=0;
BEGIN
    SELECT Salary INTO SAL FROM Employees WHERE Employee_ID = EMP_ID;
    RETURN SAL;
END;
--SET SERVEROUTPUT ON;
EXECUTE DBMS_OUTPUT.PUT_LINE(GET_SAL(100));
SELECT First_Name, GET_SAL(Employee_ID), SALARY FROM Employees;

--------------------------------
Create or Replace function ADD_TAX(val IN NUMBER)
RETURN NUMBER
IS
Begin
    Return (val * 0.06);
END ADD_TAX;
---------
SELECT Employee_ID,LAST_NAME, Salary, ADD_TAX(Salary)
from Employees
where department_id = 100;
 
---------------------------------
CREATE OR REPLACE FUNCTiON GET_DEPNAME(DEP_ID Employees.Department_ID%TYPE) -- type "function"s "i" lowercase, SqlDeveloper BUG!!!
RETURN VARCHAR2                                                         
IS
    DEPNAME departments.department_name%TYPE :='UnKnown';
BEGIN
    SELECT Department_NAME INTO DEPNAME FROM Departments WHERE Department_ID = DEP_ID;
    RETURN DEPNAME;
END GET_DEPNAME;

SELECT Employee_ID,LAST_NAME, Salary, ADD_TAX(Salary),GET_DEPNAME(Department_ID)
from Employees
where Employee_ID = 100;

------------------------------------------
CREATE OR REPLACE FUNCTiON GET_JOB_TITLE(JOBID IN Jobs.JOB_ID%TYPE) -- type "function"s "i" lowercase, SqlDeveloper BUG!!!
RETURN Jobs.JOB_TITLE%Type                                                       
IS
    Title Jobs.JOB_TITLE%Type;
BEGIN
    SELECT JOB_TITLE INTO Title FROM Jobs WHERE JOB_ID = JOBID;
    RETURN Title;
END GET_JOB_TITLE;

SELECT First_Name,LAST_NAME, GET_JOB_TITLE('AD_VP')
from Employees;


