--IF THEN
--CASE WHEN


SELECT Salary,
    CASE
     WHEN Salary < 2000 THEN 'Level 1'
     WHEN Salary < 3000 THEN 'Level 2'
     WHEN Salary < 4000 THEN 'Level 3'
     WHEN Salary < 5000 THEN 'Level 4'
     WHEN Salary <= 27500 THEN 'Level 5'
    END
FROM Employees;


----------------
--SET SERVEROUTPUT ON;
--SET FEEDBACK OFF;
DECLARE 
    Note Varchar2(1) := 'A';
BEGIN
    CASE Note
     WHEN 'A' THEN DBMS_OUTPUT.PUT_LINE('Perfect');
     WHEN 'B' THEN DBMS_OUTPUT.PUT_LINE('Very Good');
     WHEN 'C' THEN DBMS_OUTPUT.PUT_LINE('Good');
     WHEN 'D' THEN DBMS_OUTPUT.PUT_LINE('Not Bad');
     WHEN 'E' THEN DBMS_OUTPUT.PUT_LINE('Terrible');
     ELSE DBMS_OUTPUT.PUT_LINE('Incorrect');
    END CASE;
END;

--------------------------------------------
DECLARE
    --JobID VARCHAR2(10 byte);
    --JobID Employees%RowType; -- a row type, fully
    JobID  Employees.Job_ID%TYPE ;  --Dynamic type defining.
    EmpID NUMBER :=104;
BEGIN
    SELECT Job_ID INTO JobID FROM Employees WHERE Employee_ID = EmpID;
    CASE
     WHEN JobID = 'IT_PROG' THEN DBMS_OUTPUT.PUT_LINE('IT DEPARTMENT');
     WHEN JobID = 'AD_VP' THEN DBMS_OUTPUT.PUT_LINE('ADM DEPARTMENT');
     WHEN JobID = 'FI_MGR' THEN DBMS_OUTPUT.PUT_LINE('F�nance Mngmnt DEPARTMENT');
     ELSE DBMS_OUTPUT.PUT_LINE('Other Deps');
    END CASE;
END;
--------------------
DECLARE
    EmploRow Employees%RowType;
BEGIN
    EmploRow.First_Name := 'Codefx';
    EmploRow.Last_Name := 'OracLe'; 
    DBMS_OUTPUT.PUT_LINE(EmploRow.First_Name ||' '||  EmploRow.Last_Name );
END;











     
     